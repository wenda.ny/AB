﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;

public class Force : Bird
{
   // public float Health = 50f;
    public GameObject deathEffect;
   

    public UnityAction<GameObject> OnEnemyDestroyed = delegate { };

    private bool _isHit = false;

    void OnDestroy()
    {
        if (_isHit)
        {
           // Instantiate(deathEffect, transform.position, Quaternion.identity);
            OnEnemyDestroyed(gameObject);
        }
    }

    public override void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.GetComponent<Rigidbody2D>() == null) return;

        if (col.gameObject.tag == "Enemy" || col.gameObject.tag == "Obstacle" || col.gameObject.tag == "bg")
        {
            Debug.Log("hit dari force");
            _isHit = true;
           

            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, radius);
           // foreach (Collider2D nearbyObject in colliders)
              //  Destroy(gameObject);
            // Instantiate(deathEffect, transform.position, Quaternion.identity);
            // Destroy(gameObject);
            //
            Vector2 direction = (col.transform.position - transform.position).normalized;

            Quaternion rotation = Quaternion.LookRotation(direction);
            GameObject anim= Instantiate(deathEffect, transform.position, Quaternion.identity);
            for (int i=0;i<5;i++) { 
            //foreach (Collider2D nearbyObject in colliders) { 
                // Instantiate(bulu, transform.position, rotation);
                Instantiate(deathEffect, transform.position, rotation);
                //Collider[] colid = Physics.OverlapBox(transform.position, rotation);

             
            }
            
            
            Destroy(anim, 3f);
            Destroy(gameObject);
            //Destroy(deathEffect);
            //Destroy(bulu);


        }
        
      /*  else if (col.gameObject.tag == "Obstacle")
        {
            //Hitung damage yang diperoleh
            float damage = col.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude * 10;
           // Health -= damage;

           // if (Health <= 0)
           // {
                _isHit = true;
                Destroy(gameObject);
           // }
        }*/
        
    }

}
