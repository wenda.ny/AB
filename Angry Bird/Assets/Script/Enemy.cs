﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;

public class Enemy : MonoBehaviour
{
    public float Health = 50f;

    public AudioSource audio;

   // public AudioClip[] pig_noises;
   // private AudioClip pig_noise;

  //  public AudioClip[] death_noises;
   public AudioClip died;
    public GameObject gm;


    public UnityAction<GameObject> OnEnemyDestroyed = delegate { };

    private bool _isHit = false;

    void Start()
    {
        //Invoke("PlayPigNoises", (UnityEngine.Random.Range(5f, 17f)));
        audio = GetComponent<AudioSource>();
    }
    void OnDestroy()
    {
        if (_isHit)
        {
            OnEnemyDestroyed(gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.GetComponent<Rigidbody2D>() == null) return;

        if (col.gameObject.tag == "Bird")
        {
            _isHit = true;
            audio.PlayOneShot(died);
            Destroy(gameObject);
        }
        else if (col.gameObject.tag == "Obstacle")
        {
            //Hitung damage yang diperoleh
            float damage = col.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude * 10;
            Health -= damage;

            if (Health <= 0)
            {
                _isHit = true;
                Destroy(gameObject);
                gm.GetComponent<GameController>().score += 100;
            }
        }
    }

    //public void PlayPigNoises()
  //  {
   //     float delay = UnityEngine.Random.Range(5f, 17f);
   //     int index = UnityEngine.Random.Range(0, pig_noises.Length);
    //    pig_noise = pig_noises[5];
    //    audio.PlayOneShot(pig_noise);
    //    Invoke("PlayPigNoises", delay);
   // }

}
