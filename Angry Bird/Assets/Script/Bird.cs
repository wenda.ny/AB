﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[RequireComponent(typeof(AudioSource))]
public class Bird : MonoBehaviour
{
    public enum BirdState { Idle, Thrown, HitSomething }
    //   public GameObject Parent;
    public Rigidbody2D RigidBody;
    public CircleCollider2D Collider;
    public GameObject death;
    public UnityAction OnBirdDestroyed = delegate { };
    public UnityAction<Bird> OnBirdShot = delegate { };
    public float radius = 5f;
    public float force = 700f;
    public BirdState State { get { return _state; } }
    public AudioClip bird_launch;
    public AudioClip bird_hit;
    public AudioClip failed_level;
    public AudioSource audio;

    private BirdState _state;
    private float _minVelocity = 0.05f;
    private bool _flagDestroy = false;

    public float r = 5.0F;
    public float power = 10.0F;
    void Start()
    {
        audio = GetComponent<AudioSource>();
        RigidBody.bodyType = RigidbodyType2D.Kinematic;
        Collider.enabled = false;
        _state = BirdState.Idle;
      
    }

    void FixedUpdate()
    {
        if (_state == BirdState.Idle &&
            RigidBody.velocity.sqrMagnitude >= _minVelocity)
        {
            _state = BirdState.Thrown;
        }

        if ((_state == BirdState.Thrown || _state == BirdState.HitSomething) &&
            RigidBody.velocity.sqrMagnitude < _minVelocity &&
            !_flagDestroy)
        {
            //Hancurkan gameobject setelah 2 detik
            //jika kecepatannya sudah kurang dari batas minimum
            // 
            // Meledak();
            //Destroy(gameObject);
            ForceMode2D mode = ForceMode2D.Force;
            float explosionForce = 30F;
            float delay = UnityEngine.Random.Range(5f, 17f);
            Vector2 explosionPosition = transform.position;
            Rigidbody2D rb = gameObject.GetComponent<Rigidbody2D>(); ;
            float upwardsModifier = 0.0F;
            var explosionDir = rb.position - explosionPosition;
            var explosionDistance = explosionDir.magnitude;

            //Vector2 explosionPosition = transform.position;
            //Rigidbody2D rb = gameObject.GetComponent<Rigidbody2D>(); ;
            Vector2 explosionPos = transform.position;

         //   float upwardsModifier = 0.0F;
            //var explosionDir = rb.position - explosionPosition;
           // var explosionDistance = explosionDir.magnitude;

            // Normalize without computing magnitude again
            if (upwardsModifier == 0)
                explosionDir /= explosionDistance;
            else
            {
                // From Rigidbody.AddExplosionForce doc:
                // If you pass a non-zero value for the upwardsModifier parameter, the direction
                // will be modified by subtracting that value from the Y component of the centre point.
                explosionDir.y += upwardsModifier;
                explosionDir.Normalize();
            }
            Collider2D[] colliders = Physics2D.OverlapCircleAll(explosionPos, radius);
           // foreach (Collider2D nearbyObject in colliders)
                // rb.AddForce(Mathf.Lerp(0, explosionForce, (1 - explosionDistance)) * explosionDir, mode);
               // for (int i = 0; i < 5; i++)
          //  {
                Instantiate(death, transform.position, Quaternion.identity);
               // RigidBody.(Mathf.Lerp(0, explosionForce, (1 - explosionDistance)) * explosionDir, mode);
          //  }
            _flagDestroy = true;
            StartCoroutine(DestroyAfter(3));
            Destroy(gameObject);
        }

      //  AddExplosionForce(this Rigidbody2D rb, float explosionForce, Vector2 explosionPosition, float explosionRadius, float upwardsModifier = 0.0F, ForceMode2D mode = ForceMode2D.Force);



    }

    private IEnumerator DestroyAfter(float second)
    {
        yield return new WaitForSeconds(second);
        Destroy(gameObject);
    }

    public void MoveTo(Vector2 target, GameObject parent)
    {
        gameObject.transform.SetParent(parent.transform);
        gameObject.transform.position = target;
        
    }

    public void Shoot(Vector2 velocity, float distance, float speed)
    {
        Collider.enabled = true;
        RigidBody.bodyType = RigidbodyType2D.Dynamic;
        RigidBody.velocity = velocity * speed * distance;
        OnBirdShot(this);
        audio.PlayOneShot(bird_launch);


    }

    void OnDestroy()
    {
        if (_state == BirdState.Thrown || _state == BirdState.HitSomething)
            OnBirdDestroyed();
    }


    public virtual void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("hit");
        audio.PlayOneShot(bird_hit);
        _state = BirdState.HitSomething;
       
        //Meledak();



    }

    /*  void Meledak1()
      {

          Vector2 explosionPos = transform.position;
          Collider2D[] colliders = Physics2D.OverlapCircleAll(explosionPos, radius);
          foreach (Collider2D nearbyObject in colliders)
          {
              Rigidbody2D rb = nearbyObject.GetComponent<Rigidbody2D>();
            if (nearbyObject.gameObject.tag == "Enemy")
                 nearbyObject.rigidbody.AddExplosionForce(power, explosionPos, radius, 3.0F);
                
          }
      }*/

    public virtual void OnTap()
    {
        //Do nothing
    }
    /*
    public virtual void Meledak()
    {
        Instantiate(death, transform.position, transform.rotation);
        Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position, radius);

        foreach (Collider2D nearbyObject in collider)
       //for(int i=0;i<10;i++)
        {
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if (rb != null || nearbyObject.gameObject.tag == "Enemy" || nearbyObject.gameObject.tag == "Obstacle")
            {
                rb.AddExplosionForce(force, transform.position, radius, 3.0F);
            }
            Destroyer dest = nearbyObject.GetComponent<Destroyer>();
            if (dest != null)
            {
                dest.Destroy();
            }
        }
        Destroy(gameObject);
    }
    /*
        public void meledak1()
          //  public void AddExplosionForce(this Rigidbody2D rb, float explosionForce, Vector2 explosionPosition, float explosionRadius, float upwardsModifier = 0.0F, ForceMode2D mode = ForceMode2D.Force)
            {
            ForceMode2D mode = ForceMode2D.Force;
            float explosionForce = 30F;
            Vector2 explosionPosition= transform.position;
            Rigidbody2D rb= gameObject.GetComponent<Rigidbody2D>(); ;
            float upwardsModifier = 0.0F;
                var explosionDir = rb.position - explosionPosition;
                var explosionDistance = explosionDir.magnitude;

                // Normalize without computing magnitude again
                if (upwardsModifier == 0)
                    explosionDir /= explosionDistance;
                else
                {
                    // From Rigidbody.AddExplosionForce doc:
                    // If you pass a non-zero value for the upwardsModifier parameter, the direction
                    // will be modified by subtracting that value from the Y component of the centre point.
                    explosionDir.y += upwardsModifier;
                    explosionDir.Normalize();
                }

                 rb.AddForce(Mathf.Lerp(0, explosionForce, (1 - explosionDistance)) * explosionDir, mode);
          //  }


            }*/
}
