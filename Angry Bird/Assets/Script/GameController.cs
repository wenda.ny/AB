﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
[RequireComponent(typeof(AudioSource))]
public class GameController : MonoBehaviour
{
    public SlingShooter SlingShooter;
    public TrailController TrailController;
    public List<Bird> Birds;
    public List<Enemy> Enemies;
    public AudioClip failed_level;
    public AudioClip level_complete;
    private AudioSource audio;
    public int score = 0;

    private Bird _shotBird;
    public BoxCollider2D TapCollider;
    private int lvl = 0;
    private bool _isGameEnded = false;

    void Start()
    {
        audio = GetComponent<AudioSource>();

        for (int i = 0; i < Birds.Count; i++)
        {
            Birds[i].OnBirdDestroyed += ChangeBird;
            Birds[i].OnBirdShot += AssignTrail;
        }

        for (int i = 0; i < Enemies.Count; i++)
        {
            Enemies[i].OnEnemyDestroyed += CheckGameEnd;
        }

        TapCollider.enabled = false;
        SlingShooter.InitiateBird(Birds[0]);
        _shotBird = Birds[0];
    }

    public void ChangeBird()
    {

        TapCollider.enabled = false;
        if (_isGameEnded)
        {
            return;
        }
        Birds.RemoveAt(0);
        if (Birds.Count > 0)
            SlingShooter.InitiateBird(Birds[0]);
        _shotBird = Birds[0];
    }

    public void CheckGameEnd(GameObject destroyedEnemy)
    {
        for (int i = 0; i < Enemies.Count; i++)
        {
            if (Enemies[i].gameObject == destroyedEnemy)
            {
                Enemies.RemoveAt(i);
               // score += 10;
                break;
            }
        }

        if (Enemies.Count == 0)
        {
            _isGameEnded = true;
            audio.PlayOneShot(level_complete);
            Delay1();
            Debug.Log("YOU WIN!");
            lvl++;
            //Reload();
            //  lvl++;
            //GoToScene(Scene)
            //Application.loadedLevel("Level 1");
            // WaitForSeconds(5f);
            SceneManager.LoadScene(lvl);
            if (lvl > 1)
            {
                lvl = 1;
            }
            //TapCollider.enabled = false;
        }
        else
        {
            Debug.Log("Kalah");
            Reload();
        }
    }
    public void AssignTrail(Bird bird)
    {
        TrailController.SetBird(bird);
        StartCoroutine(TrailController.SpawnTrail());
        TapCollider.enabled = true;
    }

    void OnMouseUp()
    {
        if (_shotBird != null)
        {
            _shotBird.OnTap();
        }
    }

    IEnumerator Reload()
    {
        yield return new WaitForSeconds(2f);
        audio.PlayOneShot(failed_level);
        Debug.Log("Reloading");

        yield return new WaitForSeconds(5f);

        //GoToScene(Scene)
        //Application.loadedLevel("Level 1");
        //yield return new WaitForSeconds(5f);

        // if (lvl > 1)
        // {
       // lvl = 1;
        // }
        //SceneManager.LoadScene(lvl);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    IEnumerator Delay1()
    {
        yield return new WaitForSeconds(5f);

    }
}
